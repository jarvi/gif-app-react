import React, { useState } from "react"

export const AddCategory = ({ setFuntion }) => {
  const [inputValue, setInputValue] = useState("dos")

  const handleInputChange = e => {
    setInputValue(e.target.value)
  }

  const handleSubmit = e => {
    e.preventDefault()
    console.log("submit realizado", this)
    if (inputValue.trim().length > 1) {
      setFuntion(cats => {
        return [inputValue, ...cats]
      })
      setInputValue("")
    }
  }

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" value={inputValue} onChange={handleInputChange} />
    </form>
  )
}
