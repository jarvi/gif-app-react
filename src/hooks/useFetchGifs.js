import { useState, useEffect } from "react"
import { getGifs } from "../helper/getGifs"

export const useFetchGifs = selectCategory => {
  const [state, setState] = useState({
    data: [],
    loading: true,
  })

  useEffect(
    _ => {
      getGifs(selectCategory).then(imgs => {
        // setTimeout(() => {
        setState({
          data: imgs,
          loading: false,
        })
        //  }, 3000)
      })
    },
    [selectCategory]
  )

  return state
}
