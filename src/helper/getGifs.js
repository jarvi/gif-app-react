export const getGifs = async category => {
  let url = `https://api.giphy.com/v1/gifs/search?api_key=ZvefO7ieZyxLyebvNcoCaWqqfgQPMhXQ&q=${encodeURI(
    category
  )}&limit=1`

  const response = await fetch(url)
  const data = await response.json()
  const gifs = data.data.map(({ id, title, images }) => {
    return {
      id: id,
      title: title,
      url: images?.downsized.url,
    }
  })
  console.log(gifs)
  return gifs
}
