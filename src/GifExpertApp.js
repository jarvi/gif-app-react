import React, { useState } from "react"
import { AddCategory } from "./components/AddCategory"
import { GifGrid } from "./components/GifGrid"

export const GifExpertApp = () => {
  const [categories, setCategories] = useState([""])

  return (
    <div>
      <h1>gif</h1>
      <AddCategory setFuntion={setCategories} />
      <hr />
      {categories.map((category, i) => (
        <GifGrid category={category} key={i} />
      ))}
    </div>
  )
}
